FROM node:latest
WORKDIR /usr/src/app
COPY . /usr/src/app
RUN npm install -g pnpm
RUN pnpm install && \
    pnpm build 

RUN rm -rf node_modules

EXPOSE 3000
RUN chown -R node /usr/src/app
USER node
CMD ["npm", "start"]
