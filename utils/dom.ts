// 判断俩个元素是否重叠

export function areElementsOverlapping(element1: HTMLElement, element2: HTMLElement): Promise<boolean> {
    return new Promise(resolve => {
      const observer = new IntersectionObserver(entries => {
        for (const entry of entries) {
          if (entry.isIntersecting) {
            // Elements are overlapping
            observer.disconnect();
            resolve(true);
            return;
          }
        }
        // Elements are not overlapping
        observer.disconnect();
        resolve(false);
      });
  
      observer.observe(element1);
      observer.observe(element2);
    });
  }