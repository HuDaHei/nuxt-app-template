// https://nuxt.com/docs/api/configuration/nuxt-config
// eslint-disable-next-line no-undef
export default defineNuxtConfig({
  devtools: { enabled: true },
  modules: [
    "@nuxtjs/eslint-module",
    "@nuxtjs/stylelint-module",
    "@unocss/nuxt",
    "@ant-design-vue/nuxt"
  ],
  build: {
    transpile: ["gsap"],
  },
  css: ["@unocss/reset/normalize.css"],
  app: {
    head: {
      script: [
        {
          src: "https://lf1-cdn-tos.bytegoofy.com/obj/iconpark/svg_26922_25.1cc4fd45b97d6a890c955c5ae7bcb3a6.js",
        },
        { src: "https://unpkg.com/swiper@8/swiper-bundle.min.js" },
      ],
      link: [
        {
          href: "//at.alicdn.com/t/c/font_4158496_w4xv6g2yi4.css",
          rel: "stylesheet",
        },
        {
          href: "https://unpkg.com/swiper@8/swiper-bundle.min.css",
          rel: "stylesheet",
        },
      ],
    },
  },
  components: [
    {
      path: "~/components/",
      pathPrefix: false,
    },
  ],
});
