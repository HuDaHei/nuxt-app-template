// uno.config.ts
import { defineConfig, presetWind } from "unocss";
import { presetAttributify, presetIcons } from "unocss";

export default defineConfig({
  // ...UnoCSS 选项
  presets: [
    presetWind(),
    presetAttributify(),
    presetIcons({
      collections: {
        gis: () =>
          import("@iconify-json/gis/icons.json").then((p) => p.default),
      },
    }),
  ],
  theme: {
     colors: {
        'primary': '#008890',
        'second': '#fdb078',
        'th': '#b7b7b7',
        'four': '#797979',
        "d8": '#d8d8d8',
        'brand': {
          'primary': '#008890',
        }
     }
  }
});
